$(document).ready(function () {
      
  });

  function validateForm(){

    var patient = document.getElementById("namePatient").value;
    var phone = document.getElementById("phonePatient").value;
    var email = document.getElementById("emailPatient").value;
    var hour = document.getElementById("hourDate").value;
    var dateD = document.getElementById("dateDate").value;

    if (!document.getElementById("namePatient").checkValidity() || patient == "") {
      Swal.fire({
        title: 'Error!',
        text: 'Verifica la informacion del campo Nombre',
        icon: 'error',
        confirmButtonText: 'Cool'
      });
      return true
    }

    if (!document.getElementById("phonePatient").checkValidity() || phone == "") {
      Swal.fire({
        title: 'Error!',
        text: 'Verifica la informacion del campo Teléfono',
        icon: 'error',
        confirmButtonText: 'Cool'
      });
      return true
    }

    if (!document.getElementById("emailPatient").checkValidity() || email == "") {
      Swal.fire({
        title: 'Error!',
        text: 'Verifica la informacion del campo Correo',
        icon: 'error',
        confirmButtonText: 'Cool'
      });
      return true
    }

    if (!document.getElementById("hourDate").checkValidity() || hour == "") {
      Swal.fire({
        title: 'Error!',
        text: 'Verifica la informacion del campo Hora',
        icon: 'error',
        confirmButtonText: 'Cool'
      });
      return true
    }

    if (!document.getElementById("dateDate").checkValidity() || dateD == "") {
      Swal.fire({
        title: 'Error!',
        text: 'Verifica la informacion del campo Fecha',
        icon: 'error',
        confirmButtonText: 'Cool'
      });
      return true
    }
  }

  $('#form_addDate').on('submit', function(event){
    event.preventDefault();

    let response = validateForm();

    if(response){
      return
    }

    var formData = new FormData(this);

    $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
    $.ajax({
      url :"saveDate",
      method: "POST",
      data : formData,
      dataType:'JSON',
      processData: false,
      contentType: false,
      success:function(data)
      {
          if(data.error){
              $('#exampleModal').modal('hide');

                  Swal.fire({
                      title: 'Error!',
                      text: 'Ocurrio un error intenta mas tarde',
                      confirmButtonText: 'Ok'
                    });
              return

          }else{
              $('#exampleModal').modal('hide');

              Swal.fire({
                title: 'Exito',
                text: data.message,
                icon: 'info',
                confirmButtonColor: '#3085d6',
                confirmButtonText: 'Ok'
              }).then((result) => {
                if (result.isConfirmed) {
                  
                  location.reload();

                }
              });

          return

          }

      },
      error: function (data) {
        Swal.fire({
          title: 'Error!',
          text: 'Ocurrio un error intenta mas tarde',
          confirmButtonText: 'Ok'
        });
      }
    });
  }); 

  function openModalDate(){

    $("#saveDateBtn").show();
    $("#editDateBtn").hide();
    $("#deleteDateBtn").hide();
    document.getElementById('form_addDate').reset();

    $( "#namePatient" ).prop( "disabled", false );
    $( "#phonePatient" ).prop( "disabled", false );
    $( "#emailPatient" ).prop( "disabled", false );
    $( "#hourDate" ).prop( "disabled", false );
    $( "#dateDate" ).prop( "disabled", false );
    $( "#idDate" ).prop( "disabled", false );

    $('#exampleModal').modal('show');

  }

  function edit(p){

    $("#saveDateBtn").hide();
    $("#editDateBtn").show();
    $("#deleteDateBtn").show();

    $( "#namePatient" ).prop( "disabled", false );
    $( "#phonePatient" ).prop( "disabled", false );
    $( "#emailPatient" ).prop( "disabled", false );
    $( "#hourDate" ).prop( "disabled", false );
    $( "#dateDate" ).prop( "disabled", false );
    $( "#idDate" ).prop( "disabled", false );

    $.ajax({
      url :"getDate/"+p,
      method: "get",
      dataType:'JSON',
      processData: false,
      contentType: false,
      success:function(data)
      {
          if(data.error){
              $('#exampleModal').modal('hide');

          }else{

            $('#namePatient').val(data.data.user);
            $('#phonePatient').val(data.data.phone);
            $('#emailPatient').val(data.data.email);
            $('#hourDate').val(data.data.hour);
            $('#dateDate').val(data.data.date_prog);
            $('#idDate').val(data.data.id);

            $('#exampleModal').modal('show');

          }

      },
      error: function (data) {
        Swal.fire({
          title: 'Error!',
          text: 'Ocurrio un error intenta mas tarde',
          confirmButtonText: 'Ok'
        });
      }
    });

  }


  function onlyShow(p){

    $("#saveDateBtn").hide();
    $("#editDateBtn").hide();
    $("#deleteDateBtn").hide();

    $( "#namePatient" ).prop( "disabled", true );
    $( "#phonePatient" ).prop( "disabled", true );
    $( "#emailPatient" ).prop( "disabled", true );
    $( "#hourDate" ).prop( "disabled", true );
    $( "#dateDate" ).prop( "disabled", true );
    $( "#idDate" ).prop( "disabled", true );

    document.getElementById("labelModalTitle").innerHTML = "Informacón de Cita";

    $.ajax({
      url :"getDate/"+p,
      method: "get",
      dataType:'JSON',
      processData: false,
      contentType: false,
      success:function(data)
      {
          if(data.error){
              $('#exampleModal').modal('hide');

          }else{

            $('#namePatient').val(data.data.user);
            $('#phonePatient').val(data.data.phone);
            $('#emailPatient').val(data.data.email);
            $('#hourDate').val(data.data.hour);
            $('#dateDate').val(data.data.date_prog);
            $('#idDate').val(data.data.id);

            $('#exampleModal').modal('show');

          }

      },
      error: function (data) {
        Swal.fire({
          title: 'Error!',
          text: 'Ocurrio un error intenta mas tarde',
          confirmButtonText: 'Ok'
        });
      }
    });

  }

  function editDate(){

    validateForm();

    var deteid = document.getElementById("idDate").value;
    var nameP = document.getElementById("namePatient").value;
    var phoneP = document.getElementById("phonePatient").value;
    var emailP = document.getElementById("emailPatient").value;
    var hourDate = document.getElementById("hourDate").value;
    var dateDate = document.getElementById("dateDate").value;

    $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
    $.ajax({
      url :"updateDate/"+deteid,
      method: "put",
      data:{
        name:nameP,
        phone:phoneP,
        email:emailP,
        hour:hourDate,
        dateD:dateDate
      },
      dataType:'JSON',
      success:function(data)
      {

          if(data.error){
              $('#exampleModal').modal('hide');

                  Swal.fire({
                      title: 'Error!',
                      text: 'Ocurrio un error intenta mas tarde',
                      confirmButtonText: 'Ok'
                    });
              return

          }else{
              $('#exampleModal').modal('hide');

              Swal.fire({
                  title: 'Exito!',
                  text: data.message,
                  confirmButtonText: 'Ok'
              });

          return

          }

      },
      error: function (data) {
        Swal.fire({
          title: 'Error!',
          text: 'Ocurrio un error intenta mas tarde',
          confirmButtonText: 'Ok'
        });
      }
    });

  }

  function deleteDate(){

    var deteid = document.getElementById("idDate").value;

    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: false
    })
    
    swalWithBootstrapButtons.fire({
      title: '¡Eliminar!',
      text: "Seguro de eliminar la cita?",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Si, eliminar!',
      cancelButtonText: 'No, cancelar!',
      reverseButtons: true
    }).then((result) => {

      if (result.isConfirmed) {

        $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
        });
        $.ajax({
          url :"deleteDate/"+deteid,
          method: "delete",
          dataType:'JSON',
          success:function(data)
          {
    
              if(data.error){
                  $('#exampleModal').modal('hide');
    
                      Swal.fire({
                          title: 'Error!',
                          text: 'Ocurrio un error intenta mas tarde',
                          confirmButtonText: 'Ok'
                        });
                  return
    
              }else{
                  $('#exampleModal').modal('hide');

                  Swal.fire({
                    title: 'Exito',
                    text: data.message,
                    icon: 'info',
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: 'Ok'
                  }).then((result) => {
                    if (result.isConfirmed) {
                      
                      location.reload();

                    }
                  });
    
              return
    
              }
    
          },
          error: function (data) {
            Swal.fire({
              title: 'Error!',
              text: 'Ocurrio un error intenta mas tarde',
              confirmButtonText: 'Ok'
            });
          }
        });

      } else if (
        /* Read more about handling dismissals below */
        result.dismiss === Swal.DismissReason.cancel
      ) {
        swalWithBootstrapButtons.fire(
          'Cancelled',
          'Your imaginary file is safe :)',
          'error'
        )
      }
    });




  }