$(document).ready(function () {
      
});

function openModalConsult(p){
    
    $("#editConsultBtn").hide();
    $("#saveConsultBtn").show();
    $("#deleteConsultBtn").hide();
    $("#saveChangetBtn").hide();

    document.getElementById('form_consult').reset();
    
    $( "#mConsult" ).prop( "disabled", false );
    $( "#observations" ).prop( "disabled", false );
    $( "#recomendations" ).prop( "disabled", false );
    
    $.ajax({
        url :"getDate/"+p,
        method: "get",
        dataType:'JSON',
        processData: false,
        contentType: false,
        success:function(data)
        {
            if(data.error){
                $('#consultModal').modal('hide');
  
            }else{
  
            document.getElementById("nameP").innerHTML = data.data.user;
            document.getElementById("phoneP").innerHTML = data.data.phone;
            document.getElementById("emailP").innerHTML = data.data.email;
            document.getElementById("hourDate").innerHTML = data.data.hour;
            document.getElementById("dateDate").innerHTML = data.data.date_prog;
              $('#idConsult').val(data.data.id);
  
              $('#consultModal').modal('show');
  
            }
  
        },
        error: function (data) {
          Swal.fire({
            title: 'Error!',
            text: 'Ocurrio un error intenta mas tarde',
            confirmButtonText: 'Ok'
          });
        }
      });
}

function openModalConsultView(p){

  $("#editConsultBtn").show();
  $("#saveConsultBtn").hide();
  $("#deleteConsultBtn").show();
  $("#saveChangetBtn").hide();
  
  $.ajax({
      url :"getConsult/"+p,
      method: "get",
      dataType:'JSON',
      processData: false,
      contentType: false,
      success:function(data)
      {

          if(data.error){
              $('#consultModal').modal('hide');

          }else{

            $( "#mConsult" ).prop( "disabled", true );
            $( "#observations" ).prop( "disabled", true );
            $( "#recomendations" ).prop( "disabled", true );

            document.getElementById("nameP").innerHTML = data.data.user;
            document.getElementById("phoneP").innerHTML = data.data.phone;
            document.getElementById("emailP").innerHTML = data.data.email;
            document.getElementById("hourDate").innerHTML = data.data.hour;
            document.getElementById("dateDate").innerHTML = data.data.date_prog;
            $("#mConsult").val(data.data.date_cons.reason);
            $("#observations").val(data.data.date_cons.observations);
            $("#recomendations").val(data.data.date_cons.recomendations);
            $('#idConsult').val(data.data.id);
            $('#idConsultDate').val(data.data.date_cons.id);

            $('#consultModal').modal('show');

          }

      },
      error: function (data) {
        Swal.fire({
          title: 'Error!',
          text: 'Ocurrio un error intenta mas tarde',
          confirmButtonText: 'Ok'
        });
      }
    });
}

$('#form_consult').on('submit', function(event){
    event.preventDefault();

    let response = validateForm();

    if(response){
      return
    }

    var formData = new FormData(this);
    $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
    $.ajax({
      url :"saveConsult",
      method: "POST",
      data : formData,
      dataType:'JSON',
      processData: false,
      contentType: false,
      success:function(data)
      {
          if(data.error){
              $('#consultModal').modal('hide');

                  Swal.fire({
                      title: 'Error!',
                      text: 'Ocurrio un error intenta mas tarde',
                      confirmButtonText: 'Ok'
                    });
              return

          }else{
              $('#consultModal').modal('hide');

              Swal.fire({
                title: 'Exito',
                text: data.message,
                icon: 'info',
                confirmButtonColor: '#3085d6',
                confirmButtonText: 'Ok'
              }).then((result) => {
                if (result.isConfirmed) {
                  
                  location.reload();

                }
              });
          return

          }

      },
      error: function (data) {
        Swal.fire({
          title: 'Error!',
          text: 'Ocurrio un error intenta mas tarde',
          confirmButtonText: 'Ok'
        });
      }
    });

});

function editConsult(){

  $( "#mConsult" ).prop( "disabled", false );
  $( "#observations" ).prop( "disabled", false );
  $( "#recomendations" ).prop( "disabled", false );
  $("#editConsultBtn").hide();
  $("#saveChangetBtn").show();

}

function saveChange(){

  let response = validateForm();

  if(response){
    return
  }

  var consultId = document.getElementById("idConsult").value;
  var mConsult = document.getElementById("mConsult").value;
  var observations = document.getElementById("observations").value;
  var recomendations = document.getElementById("recomendations").value;

  $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });
  $.ajax({
    url :"updateConsult/"+consultId,
    method: "put",
    data:{
      reason:mConsult,
      observations:observations,
      recomendations:recomendations,
    },
    dataType:'JSON',
    success:function(data)
    {

        if(data.error){
            $('#consultModal').modal('hide');

                Swal.fire({
                    title: 'Error!',
                    text: 'Ocurrio un error intenta mas tarde',
                    confirmButtonText: 'Ok'
                  });
            return

        }else{
            $('#consultModal').modal('hide');

            Swal.fire({
              title: 'Exito',
              text: data.message,
              icon: 'info',
              confirmButtonColor: '#3085d6',
              confirmButtonText: 'Ok'
            }).then((result) => {
              if (result.isConfirmed) {
                
                location.reload();

              }
            });

        return

        }

    },
    error: function (data) {
      Swal.fire({
        title: 'Error!',
        text: 'Ocurrio un error intenta mas tarde',
        confirmButtonText: 'Ok'
      });
    }
  });

}

function validateForm(){

  var mConsult = document.getElementById("mConsult").value;
  var observations = document.getElementById("observations").value;
  var recomendations = document.getElementById("recomendations").value;

  if (!document.getElementById("mConsult").checkValidity() || mConsult == "") {
    Swal.fire({
      title: 'Error!',
      text: 'Verifica la informacion del campo Motivo',
      icon: 'error',
      confirmButtonText: 'Ok'
    });
    return true
  }

  if (!document.getElementById("observations").checkValidity() || observations == "") {
    Swal.fire({
      title: 'Error!',
      text: 'Verifica la informacion del campo Observaciones',
      icon: 'error',
      confirmButtonText: 'Ok'
    });
    return true
  }

  if (!document.getElementById("recomendations").checkValidity() || recomendations == "") {
    Swal.fire({
      title: 'Error!',
      text: 'Verifica la informacion del campo Recomendaciones',
      icon: 'error',
      confirmButtonText: 'Ok'
    });
    return true
  }

}

