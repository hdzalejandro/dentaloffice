<?php

namespace App\Models;;

use Illuminate\Database\Eloquent\Model;

class CDdates extends Model
{
    
    protected $table = 'CD_date';

    protected $primaryKey = 'id';

    public $timestamps = true;

    protected $fillable = [
        'hour',
        'date_prog',
        'user',
        'phone',
        'email',
        'status'
    ];

    public function dateCons(){
        return $this->hasOne('App\Models\CDConsultation', 'date_id', 'id');
    }


}