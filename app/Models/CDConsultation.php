<?php

namespace App\Models;;

use Illuminate\Database\Eloquent\Model;

class CDConsultation extends Model
{
    
    protected $table = 'CD_consultation';

    protected $primaryKey = 'id';

    public $timestamps = true;

    protected $fillable = [
        'reason',
        'onservations',
        'recomendations',
        'date_id',
        'doctor_id'
    ];

    public function consultation(){
        return $this->belongsTo('App\Models\CDdates', 'date_id', 'id');
    }


}