<?php

namespace App\Http\Controllers;
use DB;
use App\Models\CDConsultation;
use App\Models\CDdates;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ConsultController extends Controller
{
    public function saveConsult(Request $request){

        $newConsult = new CDConsultation();
        $newConsult->reason = $request->mConsult;
        $newConsult->observations = $request->observations;
        $newConsult->recomendations = $request->recomendations;
        $newConsult->date_id = $request->idConsult;
        $newConsult->doctor_id = $request->doctorId;
        $newConsult->save();

        $dateUp = CDdates::where('id', $request->idConsult)->first();
        $dateUp->status = 1;
        $dateUp->update();

        return response()->json([
            'error' => false,
            'message' => 'Consulta Creada',
            'data' => $request->doctorId
        ]);

    }

    public function getConsult($id){

        $consult = CDdates::with('dateCons')->where('id', $id)->first();

        return response()->json([
            'error' => false,
            'message' => 'Consulta',
            'data' => $consult
        ]);

    }

    public function updateConsult(Request $request, $id){

        $consultUp = CDConsultation::where('date_id', $id)->first();
        $consultUp->reason = $request->reason;
        $consultUp->observations = $request->observations;
        $consultUp->recomendations = $request->recomendations;
        $consultUp->update();

        return response()->json([
            'error' => false,
            'message' => 'Consulta Actualizada',
            'data' => $consultUp
        ]);

    }

    public function deleteConsult($id){

        $consultDelete = CDConsultation::find($id);

        $consultDelete->delete();

        return response()->json([
            'error' => false,
            'message' => 'Consulta Eliminada',
            'data' => $consultDelete
        ]);

    }

}