<?php

namespace App\Http\Controllers;
use DB;
use App\Models\CDdates;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class DateController extends Controller
{
    public function save(Request $request){

        $newDate = new CDdates();
        $newDate->user = $request->namePatient;
        $newDate->phone = $request->phonePatient;
        $newDate->email = $request->emailPatient;
        $newDate->date_prog = $request->dateDate;
        $newDate->hour = $request->hourDate;
        $newDate->save();

        return response()->json([
            'error' => false,
            'message' => 'Cita Creada',
            'data' => $newDate
        ]);
    }

    public function getDate($id){

        $infoDate = CDdates::where('id', $id)->first();

        return response()->json([
            'error' => false,
            'message' => 'Informacion de la cita',
            'data' => $infoDate
        ]);

    }

    public function updateDate(Request $request, $id){

        $dateUp = CDdates::where('id', $id)->first();
        $dateUp->user = $request->name;
        $dateUp->hour = $request->hour;
        $dateUp->date_prog = $request->dateD;
        $dateUp->phone = $request->phone;
        $dateUp->email = $request->email;
        $dateUp->update();

        return response()->json([
            'error' => false,
            'message' => 'Cita actualizada',
            'data' => $dateUp
        ]);
    }

    public function deleteDate($id){

        $dateDelete = CDdates::find($id);

        $dateDelete->delete();

        return response()->json([
            'error' => false,
            'message' => 'Cita Eliminada',
            'data' => $dateDelete
        ]);

    }
}