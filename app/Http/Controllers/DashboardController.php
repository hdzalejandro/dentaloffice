<?php

namespace App\Http\Controllers;
use DB;
use App\Models\CDdates;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class DashboardController extends Controller
{
    public function showConsults(){

        $dates = CDdates::orderBy('id', 'desc')->get();

        return view('consults/index', ['data' => $dates]);

    }

    public function showDoctors(){

        return view('doctors/index');
    }

    public function showDates(){

        $dates = CDdates::orderBy('id', 'desc')->get();

        return view('patients/index', ['data' => $dates]);
    }
}