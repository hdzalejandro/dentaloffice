<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/showPatients', 'DashboardController@showConsults')->name('Patients');
Route::get('/showDates', 'DashboardController@showDates')->name('Dates');

//dates
Route::post('/saveDate', 'DateController@save')->name('saveDate');
Route::get('/getDate/{id}', 'DateController@getDate')->name('getDate');
Route::put('/updateDate/{id}', 'DateController@updateDate')->name('updateDate');
Route::delete('/deleteDate/{id}', 'DateController@deleteDate')->name('deleteDate');


//consults
Route::post('/saveConsult', 'ConsultController@saveConsult')->name('saveConsult');
Route::get('/getConsult/{id}', 'ConsultController@getConsult')->name('getConsult');
Route::put('/updateConsult/{id}', 'ConsultController@updateConsult')->name('updateConsult');
Route::delete('/deleteConsult/{id}', 'ConsultController@deleteConsult')->name('deleteConsult');






