@extends('dashboard.master')

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card bg-light">
                <div class="row text-center text-muted">
                <h2 class="display-2">Bienvenido {{ Auth::user()->name }}</h2>
                </div>
            </div>
        </div>
    </div>
@endsection